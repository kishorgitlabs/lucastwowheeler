package RetroClient;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import APIInterface.CategoryAPI;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by system9 on 6/30/2017.
 */

public class RetroClient {

//    private static final String ROOT_URL = "http://lucastvssalesjson.brainmagicllc.com/Api/Values/";
//  before   private static final String ROOT_URL = "http://TwoWheeler.lucastvs-catalog.in/Api/Values/";
    private static final String ROOT_URL = "http://twowheeler.lucastvs.net//Api/Values/";
    /**
     * Get Retrofit Instance
     */
    Gson gson = new GsonBuilder()
            .setLenient()
            .create();
    private static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .client(okClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private static OkHttpClient okClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(45, TimeUnit.MINUTES)
                .writeTimeout(45, TimeUnit.MINUTES)
                .readTimeout(45, TimeUnit.MINUTES)
                .build();
    }
    /**
     * Get CategoryAPI Service
     *
     * @return CategoryAPI Service
     */
    public static CategoryAPI getApiService() {
        return getRetrofitInstance().create(CategoryAPI.class);
    }


}
