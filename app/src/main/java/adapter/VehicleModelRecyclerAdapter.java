package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lucas.lucas2wheeler.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import model.getvehiclemodel.GetVehicleModelData;

public class VehicleModelRecyclerAdapter extends RecyclerView.Adapter<VehicleModelRecyclerAdapter.ViewHolder> {

    private Context context;
    private List<GetVehicleModelData> modeldata;
    private ActivityChanger activityChanger;
    private String oemname;

    public VehicleModelRecyclerAdapter(Context context, List<GetVehicleModelData> model, String oem){
        this.context=context;
        this.modeldata=model;
        this.oemname=oem;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.oemmodeladapter,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        holder.modelName.setText(modeldata.get(position).getVEHICLEMODEL());


        Picasso.with(context)
                .load("http://lucastvs-catalog.in/TwoWheeler/images/App_search/"+modeldata.get(position).getVEHICLEMODELIMAGE())
                .memoryPolicy(MemoryPolicy.NO_CACHE )
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .error(R.drawable.noimage)
                .placeholder(R.drawable.progress_animation)
                .into(holder.modelImage);

        holder.make.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activityChanger.intent(oemname,modeldata.get(position).getVEHICLEMODEL());
            }
        });
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        try {
            activityChanger =(ActivityChanger)context;
        }catch (Exception e){

        }
    }

    @Override
    public int getItemCount() {
        return modeldata.size();
    }

    public interface ActivityChanger {
        public void intent(String oem,String model);

    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView modelImage;
        private TextView modelName;
        private LinearLayout make;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            modelImage=itemView.findViewById(R.id.vehicleImage);
            modelName=itemView.findViewById(R.id.vehicleName);
            make=itemView.findViewById(R.id.make);

        }
    }
}
