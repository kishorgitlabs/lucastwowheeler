package adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.lucas.lucas2wheeler.PartDetails;
import com.lucas.lucas2wheeler.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import model.pricelist.PriceLlistData;

public class PriceListAdapter extends RecyclerView.Adapter<PriceListAdapter.ViewHolder> {

    private Context context;
    private List<PriceLlistData> priceLlistDatas;
    private List<PriceLlistData> searcheddatalist;

    public PriceListAdapter(Context context, List<PriceLlistData> priceLlistData){
        this.context=context;
        this.priceLlistDatas=priceLlistData;
        searcheddatalist=new ArrayList<>();
        this.searcheddatalist.addAll(priceLlistData);
    }

    @NonNull
    @Override
    public PriceListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(context).inflate(R.layout.pricelistadapter,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(@NonNull PriceListAdapter.ViewHolder holder, final int position) {
        holder.partnumberview.setText(priceLlistDatas.get(position).getPARTNUMBER());
        holder.unitsale.setText(priceLlistDatas.get(position).getUNITOFSALE());
        holder.hsncode.setText(priceLlistDatas.get(position).getHSNCODE());
        holder.mrp.setText(priceLlistDatas.get(position).getMRP());
        holder.description.setText(priceLlistDatas.get(position).getDESCRIPTION());
        holder.gstpercentage.setText(priceLlistDatas.get(position).getmGST()+"%"+"");

        holder.pricecardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent partdetails=new Intent(context, PartDetails.class);
                partdetails.putExtra("partnumber", priceLlistDatas.get(position).getPARTNUMBER());
                partdetails.putExtra("flow","partnumber");
                partdetails.putExtra("header","Price List");
                partdetails.putExtra("selectedmodel","");
                context.startActivity(partdetails);
            }
        });
    }

    @Override
    public int getItemCount() {
        return priceLlistDatas.size();
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        priceLlistDatas.clear();
        if (charText.length() == 0) {
            priceLlistDatas.addAll(searcheddatalist);
        } else {
            for (int i = 0; i < searcheddatalist.size(); i++) {
                String partnumber = String.valueOf(searcheddatalist.get(i).getPARTNUMBER().toLowerCase());
                if (partnumber.toLowerCase(Locale.getDefault()).contains(charText.toLowerCase())) {
                    priceLlistDatas.add(searcheddatalist.get(i));
                    notifyDataSetChanged();
                }
            }
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView partnumberview,unitsale,hsncode,mrp,description,gstpercentage;
        private CardView pricecardview;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            partnumberview=itemView.findViewById(R.id.partnumberview);
            unitsale=itemView.findViewById(R.id.unitsale);
            hsncode=itemView.findViewById(R.id.hsncode);
            mrp=itemView.findViewById(R.id.mrp);
            description=itemView.findViewById(R.id.description);
            pricecardview=itemView.findViewById(R.id.pricecardview);
            gstpercentage=itemView.findViewById(R.id.gstpercentage);
        }
    }
}
