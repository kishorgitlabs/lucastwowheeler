package adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.lucas.lucas2wheeler.R;
import com.lucas.lucas2wheeler.VehicleModelFragement;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import model.getOemList.GetOemData;

public class VehicleOemlRecyclerAdapter extends RecyclerView.Adapter<VehicleOemlRecyclerAdapter.ViewHolder> {

    private Context context;
    private List<GetOemData> oem;

    public VehicleOemlRecyclerAdapter(Context context, List<GetOemData> oemname){
        this.context=context;
        this.oem=oemname;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.oemmodeladapter,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.oemName.setText(oem.get(position).getVEHICLEMAKE());
                Picasso.with(context)
//                        .load("http:\\\\"+"lucastvssalesjson.brainmagicllc.com\\Images\\Oems\\"+oem.get(position).getVEHICLEMAKEIMAGE())
                        .load("http://lucastvs-catalog.in/TwoWheeler/images/Oems/"+oem.get(position).getVEHICLEMAKEIMAGE())
                        .memoryPolicy(MemoryPolicy.NO_CACHE )
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .error(R.drawable.noimage)
                        .placeholder(R.drawable.progress_animation)
                        .into(holder.oemImage);


        holder.make.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle oemName=new Bundle();
                oemName.putString("OEM",oem.get(position).getVEHICLEMAKE());
                AppCompatActivity activity = (AppCompatActivity) view.getContext();
                FragmentManager fragmentManager=activity.getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                VehicleModelFragement vehicleModel = new VehicleModelFragement();
                vehicleModel.setArguments(oemName);
                fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragemnt,vehicleModel).addToBackStack(null).commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return oem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView oemImage;
        private TextView oemName;
        private LinearLayout make;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            oemImage=itemView.findViewById(R.id.vehicleImage);
            oemName=itemView.findViewById(R.id.vehicleName);
            make=itemView.findViewById(R.id.make);
        }
    }
}
