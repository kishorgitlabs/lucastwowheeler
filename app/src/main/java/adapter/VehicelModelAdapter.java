package adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lucas.lucas2wheeler.PartDetails;
import com.lucas.lucas2wheeler.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import model.getmodellist.ModelListImage;

public class VehicelModelAdapter extends RecyclerView.Adapter<VehicelModelAdapter.ViewHolder> {

    private  Context context;
    private List<ModelListImage> data;

    public VehicelModelAdapter(@NonNull Context context, List<ModelListImage> vehiclemodelimage) {
        this.context=context;
        this.data=vehiclemodelimage;
    }


    public void setModelList(List<ModelListImage> vehiclemodel)
    {
    this.data=vehiclemodel;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view=LayoutInflater.from(context).inflate(R.layout.vehiclemodeladapter,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.vehiclemodelname.setText(data.get(position).getVEHICLEMODEL());

        Picasso.with(context)
                .load("http://"+"lucastvs-catalog.in/TwoWheeler/images/App_search/"+data.get(position).getVEHICLEMODELIMAGE())
//                    .load("http://lucastvssalesjson.brainmagicllc.com/Images/App_search/ACTIVA.jpg")
//                    .load("http://filterjson.brainmagicllc.com/ImageUpload/partimage/LFAA 5031.PNG")
//                    .load("http://lucastvssalesjson.brainmagicllc.com//Images//App_search//DISCOVER.jpg")

                .memoryPolicy(MemoryPolicy.NO_CACHE )
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .placeholder(R.drawable.progress_animation)
                .error(R.drawable.noimage)
                .into(holder.vehiclemodelimage);

        holder.vehiclemodellayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent partdetails=new Intent(context, PartDetails.class);
                partdetails.putExtra("partnumber", data.get(position).getVEHICLEMODEL());
                partdetails.putExtra("flow","vehiclemodel");
                partdetails.putExtra("header","Vehicle Model");
                partdetails.putExtra("selectedmodel","");
                context.startActivity(partdetails);
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView vehiclemodelname;
        private ImageView vehiclemodelimage;
        private RelativeLayout vehiclemodellayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            vehiclemodelname=itemView.findViewById(R.id.vehiclemodelname);
            vehiclemodelimage=itemView.findViewById(R.id.vehiclemodelimage);
            vehiclemodellayout=itemView.findViewById(R.id.vehiclemodellayout);
        }
    }


}
