package adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lucas.lucas2wheeler.PartDetails;
import com.lucas.lucas2wheeler.R;

import java.util.List;

import model.getchildpartsdetails.GetChildPartData;

public class ServicePartsAdapter extends RecyclerView.Adapter<ServicePartsAdapter.ViewHolder> {


    private Context context;
    private List<GetChildPartData> childata;
    private String servicepartnumber;

    public ServicePartsAdapter(Context context, List<GetChildPartData> childPartData, String servicePartNumber){
        this.context=context;
        this.childata=childPartData;
        this.servicepartnumber=servicePartNumber;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.servicepartsadapter,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.sno.setText(position+1+"");
        holder.partnumber.setText(childata.get(position).getCHILDPARTNo());
        holder.nooff.setText(childata.get(position).getNoOFF());
        holder.description.setText(childata.get(position).getDESCRIPTION());

        holder.partnumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent partdetails=new Intent(context, PartDetails.class);
                partdetails.putExtra("partnumber", childata.get(position).getCHILDPARTNo());
                partdetails.putExtra("header","Service Parts");
                partdetails.putExtra("servicepartnumberoroem",servicepartnumber);
                partdetails.putExtra("flow","serviceparts");
                partdetails.putExtra("selectedmodel","");
                context.startActivity(partdetails);
            }
        });
    }
    @Override
    public int getItemCount() {
        return childata.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView sno,partnumber,description,nooff;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            sno=itemView.findViewById(R.id.sno);
            partnumber=itemView.findViewById(R.id.partnumber);
            description=itemView.findViewById(R.id.description);
            nooff=itemView.findViewById(R.id.nooff);
        }
    }
}
