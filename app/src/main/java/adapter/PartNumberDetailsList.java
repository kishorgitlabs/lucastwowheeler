package adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.lucas.lucas2wheeler.PartDetails;
import com.lucas.lucas2wheeler.R;

import java.util.List;

public class PartNumberDetailsList extends ArrayAdapter {

    private Context context;
    private List<String> partnumberdata;


    public PartNumberDetailsList(@NonNull Context context, List<String> partnumber) {
        super(context, R.layout.partadapter);
        this.context=context;
        this.partnumberdata=partnumber;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        convertView=null;
        if (convertView==null){
            convertView= LayoutInflater.from(context).inflate(R.layout.partadapter,null);

            TextView engine=convertView.findViewById(R.id.engine);
            TextView stater=convertView.findViewById(R.id.stater);
            TextView alter=convertView.findViewById(R.id.alter);
            TextView wipermotor=convertView.findViewById(R.id.wipermotor);
            TextView wipping=convertView.findViewById(R.id.wipping);
            TextView wiper_system=convertView.findViewById(R.id.wiper_system);
            TextView rearwiper=convertView.findViewById(R.id.rearwiper);
            TextView distributor=convertView.findViewById(R.id.distributor);
            TextView ignitioncoil=convertView.findViewById(R.id.ignitioncoil);

            engine.setText("Petrol");
            stater.setText(partnumberdata.get(1));
            alter.setText(partnumberdata.get(2));
            wipermotor.setText(partnumberdata.get(3));
            wipping.setText(partnumberdata.get(4));
            wiper_system.setText(partnumberdata.get(5));
            rearwiper.setText(partnumberdata.get(0));
            distributor.setText(partnumberdata.get(1));
            ignitioncoil.setText(partnumberdata.get(2));

            engine.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent partdetails=new Intent(context, PartDetails.class);
                    context.startActivity(partdetails);
                }
            });  stater.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent partdetails=new Intent(context, PartDetails.class);
                    context.startActivity(partdetails);
                }
            });  alter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent partdetails=new Intent(context, PartDetails.class);
                    context.startActivity(partdetails);
                }
            });  wipermotor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent partdetails=new Intent(context, PartDetails.class);
                    context.startActivity(partdetails);
                }
            });  wipping.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent partdetails=new Intent(context, PartDetails.class);
                    context.startActivity(partdetails);
                }
            });  wiper_system.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent partdetails=new Intent(context, PartDetails.class);
                    context.startActivity(partdetails);
                }
            });  rearwiper.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent partdetails=new Intent(context, PartDetails.class);
                    context.startActivity(partdetails);
                }
            });  distributor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent partdetails=new Intent(context, PartDetails.class);
                    context.startActivity(partdetails);
                }
            });  ignitioncoil.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent partdetails=new Intent(context, PartDetails.class);
                    context.startActivity(partdetails);
                }
            });

        }
        return convertView;
    }

    @Override
    public int getCount() {
        return 1;
    }
}
