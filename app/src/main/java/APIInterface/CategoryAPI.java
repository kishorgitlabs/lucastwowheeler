package APIInterface;

import model.enquiry.SendEnquiryJson;
import model.getOemList.GetOemList;
import model.getchildpartsdetails.GetChildParts;
import model.getcityforserviceengg.GetServiceEnggCity;
import model.getmodellist.GetModelList;
import model.getpartnolist.GetPartNo;
import model.getservicepartnolist.GetServicePartNo;
import model.getvehiclemodel.GetVehicleModelList;
import model.partdetails.PartDetailsJson;
import model.pricelist.GetPriceList;
import model.serviceengineer.GetServiceEngineer;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface CategoryAPI {

    @GET("GetPartNoList")
    Call<GetPartNo> getPartNo();

    @GET("GetServicePartNoList")
    Call<GetServicePartNo> getServiceParts();

    @GET("GetOemImage")
    Call<GetOemList> getOemList();

    @GET("GetModellist")
    Call<GetModelList> getVehicleModellist();

    @FormUrlEncoded
    @POST("servicepartsearch")
    Call<GetChildParts> getChildParts(
            @Field("PartNumber") String serviceparts

    );
//
    @FormUrlEncoded
    @POST("Partsearch")
    Call<PartDetailsJson> getPartDetails(
            @Field("PartNumber") String partnumber
    );

    @FormUrlEncoded
    @POST("GetModelDetail_Partno")
    Call<PartDetailsJson> getVehicleModelDetails(
            @Field("VEHICLE_MODEL") String partnumber
    );
    @FormUrlEncoded
    @POST("GetOem_ModelImage")
    Call<GetVehicleModelList> getVehicleModel(
            @Field("VEHICLE_MAKE") String vehilclemake
    );

    @FormUrlEncoded
    @POST("GetServiceEng_City")
    Call<GetServiceEnggCity> getServiceEnggCity(
            @Field("State") String state
    );

    @FormUrlEncoded
    @POST("GetOEMModelDetail")
    Call<PartDetailsJson> getPartDetails(
            @Field("VEHICLE_MAKE") String vehilclemake,
            @Field("VEHICLE_MODEL") String vehiclemodel
    );

    @FormUrlEncoded
    @POST("GetServiceEng")
    Call<GetServiceEngineer> getServiceEngineerDetails(
            @Field("State") String state,
            @Field("Location")String location
    );

    @FormUrlEncoded
    @POST("GetEnquiry")
    Call<SendEnquiryJson> SendEnquirty(
            @Field("Name") String name,
            @Field("Email") String email,
            @Field("Mobile") String mobile,
            @Field("City") String city,
            @Field("Partno") String partno,
            @Field("Message") String message
    );

    @GET("GetPricelist")
    Call<GetPriceList> getPriceList();
}
