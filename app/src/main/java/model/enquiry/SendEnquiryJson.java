
package model.enquiry;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class SendEnquiryJson {

    @SerializedName("data")
    private SendEnquiryData mData;
    @SerializedName("result")
    private String mResult;

    public SendEnquiryData getData() {
        return mData;
    }

    public void setData(SendEnquiryData data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
