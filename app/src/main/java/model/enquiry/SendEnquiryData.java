
package model.enquiry;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class SendEnquiryData {

    @SerializedName("City")
    private String mCity;
    @SerializedName("Email")
    private String mEmail;
    @SerializedName("Id")
    private String mId;
    @SerializedName("InsertDate")
    private String mInsertDate;
    @SerializedName("Message")
    private String mMessage;
    @SerializedName("Mobile")
    private String mMobile;
    @SerializedName("Name")
    private String mName;
    @SerializedName("Partno")
    private String mPartno;

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(String insertDate) {
        mInsertDate = insertDate;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String mobile) {
        mMobile = mobile;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPartno() {
        return mPartno;
    }

    public void setPartno(String partno) {
        mPartno = partno;
    }

}
