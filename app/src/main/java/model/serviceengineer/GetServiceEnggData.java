
package model.serviceengineer;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class GetServiceEnggData {

    @SerializedName("Email")
    private String mEmail;
    @SerializedName("Id")
    private Long mId;
    @SerializedName("Insertdate")
    private Object mInsertdate;
    @SerializedName("Location")
    private String mLocation;
    @SerializedName("Mobile")
    private String mMobile;
    @SerializedName("Name")
    private String mName;
    @SerializedName("State")
    private String mState;
    @SerializedName("Usertype")
    private String mUsertype;

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Object getInsertdate() {
        return mInsertdate;
    }

    public void setInsertdate(Object insertdate) {
        mInsertdate = insertdate;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        mLocation = location;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String mobile) {
        mMobile = mobile;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getUsertype() {
        return mUsertype;
    }

    public void setUsertype(String usertype) {
        mUsertype = usertype;
    }

}
