
package model.serviceengineer;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class GetServiceEngineer {

    @SerializedName("data")
    private GetServiceEnggData mData;
    @SerializedName("result")
    private String mResult;

    public GetServiceEnggData getData() {
        return mData;
    }

    public void setData(GetServiceEnggData data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
