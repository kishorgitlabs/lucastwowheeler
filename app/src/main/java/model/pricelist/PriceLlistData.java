
package model.pricelist;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class PriceLlistData {

    @SerializedName("CUSTOMER_PART_NUMBER")
    private String mCUSTOMERPARTNUMBER;
    @SerializedName("DESCRIPTION")
    private String mDESCRIPTION;
    @SerializedName("F_S_Tag")
    private String mFSTag;
    @SerializedName("HSN_CODE")
    private String mHSNCODE;
    @SerializedName("id")
    private String mId;
    @SerializedName("MRP")
    private String mMRP;
    @SerializedName("PART_NUMBER")
    private String mPARTNUMBER;
    @SerializedName("RATING")
    private String mRATING;
    @SerializedName("TYPE")
    private String mTYPE;
    @SerializedName("UNIT_OF_SALE")
    private String mUNITOFSALE;
    @SerializedName("VEHICLE_MAKE")
    private String mVEHICLEMAKE;
    @SerializedName("VEHICLE_MAKE_IMAGE")
    private String mVEHICLEMAKEIMAGE;
    @SerializedName("VEHICLE_MODEL")
    private String mVEHICLEMODEL;
    @SerializedName("VEHICLE_MODEL_IMAGE")
    private String mVEHICLEMODELIMAGE;
    @SerializedName("VOLT")
    private String mVOLT;

    @SerializedName("GST")
    private String mGST;
    @SerializedName("STATUS")
    private String mSTATUS;
    @SerializedName("SUPERSEDED")
    private String mSUPERSEDED;

    public String getmGST() {
        return mGST;
    }

    public void setmGST(String mGST) {
        this.mGST = mGST;
    }

    public String getmSTATUS() {
        return mSTATUS;
    }

    public void setmSTATUS(String mSTATUS) {
        this.mSTATUS = mSTATUS;
    }

    public String getmSUPERSEDED() {
        return mSUPERSEDED;
    }

    public void setmSUPERSEDED(String mSUPERSEDED) {
        this.mSUPERSEDED = mSUPERSEDED;
    }

    public String getCUSTOMERPARTNUMBER() {
        return mCUSTOMERPARTNUMBER;
    }

    public void setCUSTOMERPARTNUMBER(String cUSTOMERPARTNUMBER) {
        mCUSTOMERPARTNUMBER = cUSTOMERPARTNUMBER;
    }

    public String getDESCRIPTION() {
        return mDESCRIPTION;
    }

    public void setDESCRIPTION(String dESCRIPTION) {
        mDESCRIPTION = dESCRIPTION;
    }

    public String getFSTag() {
        return mFSTag;
    }

    public void setFSTag(String fSTag) {
        mFSTag = fSTag;
    }

    public String getHSNCODE() {
        return mHSNCODE;
    }

    public void setHSNCODE(String hSNCODE) {
        mHSNCODE = hSNCODE;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getMRP() {
        return mMRP;
    }

    public void setMRP(String mRP) {
        mMRP = mRP;
    }

    public String getPARTNUMBER() {
        return mPARTNUMBER;
    }

    public void setPARTNUMBER(String pARTNUMBER) {
        mPARTNUMBER = pARTNUMBER;
    }

    public String getRATING() {
        return mRATING;
    }

    public void setRATING(String rATING) {
        mRATING = rATING;
    }

    public String getTYPE() {
        return mTYPE;
    }

    public void setTYPE(String tYPE) {
        mTYPE = tYPE;
    }

    public String getUNITOFSALE() {
        return mUNITOFSALE;
    }

    public void setUNITOFSALE(String uNITOFSALE) {
        mUNITOFSALE = uNITOFSALE;
    }

    public String getVEHICLEMAKE() {
        return mVEHICLEMAKE;
    }

    public void setVEHICLEMAKE(String vEHICLEMAKE) {
        mVEHICLEMAKE = vEHICLEMAKE;
    }

    public String getVEHICLEMAKEIMAGE() {
        return mVEHICLEMAKEIMAGE;
    }

    public void setVEHICLEMAKEIMAGE(String vEHICLEMAKEIMAGE) {
        mVEHICLEMAKEIMAGE = vEHICLEMAKEIMAGE;
    }

    public String getVEHICLEMODEL() {
        return mVEHICLEMODEL;
    }

    public void setVEHICLEMODEL(String vEHICLEMODEL) {
        mVEHICLEMODEL = vEHICLEMODEL;
    }

    public String getVEHICLEMODELIMAGE() {
        return mVEHICLEMODELIMAGE;
    }

    public void setVEHICLEMODELIMAGE(String vEHICLEMODELIMAGE) {
        mVEHICLEMODELIMAGE = vEHICLEMODELIMAGE;
    }

    public String getVOLT() {
        return mVOLT;
    }

    public void setVOLT(String vOLT) {
        mVOLT = vOLT;
    }

}
