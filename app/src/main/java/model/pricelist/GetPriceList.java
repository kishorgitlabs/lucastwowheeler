
package model.pricelist;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class GetPriceList {

    @SerializedName("data")
    private List<PriceLlistData> mData;
    @SerializedName("result")
    private String mResult;

    public List<PriceLlistData> getData() {
        return mData;
    }

    public void setData(List<PriceLlistData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
