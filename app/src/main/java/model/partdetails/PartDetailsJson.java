
package model.partdetails;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class PartDetailsJson {

    @SerializedName("data")
    private PartDetailsData mData;
    @SerializedName("result")
    private String mResult;

    public PartDetailsData getData() {
        return mData;
    }

    public void setData(PartDetailsData data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
