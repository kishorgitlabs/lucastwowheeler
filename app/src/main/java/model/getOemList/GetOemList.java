
package model.getOemList;

import com.google.gson.annotations.SerializedName;

import java.util.List;


@SuppressWarnings("unused")
public class GetOemList {

    @SerializedName("data")
    private List<GetOemData> mData;
    @SerializedName("result")
    private String mResult;

    public List<GetOemData> getData() {
        return mData;
    }

    public void setData(List<GetOemData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
