
package model.getOemList;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class GetOemData {

    @SerializedName("VEHICLE_MAKE")
    private String mVEHICLEMAKE;
    @SerializedName("VEHICLE_MAKE_IMAGE")
    private String mVEHICLEMAKEIMAGE;

    public String getVEHICLEMAKE() {
        return mVEHICLEMAKE;
    }

    public void setVEHICLEMAKE(String vEHICLEMAKE) {
        mVEHICLEMAKE = vEHICLEMAKE;
    }

    public String getVEHICLEMAKEIMAGE() {
        return mVEHICLEMAKEIMAGE;
    }

    public void setVEHICLEMAKEIMAGE(String vEHICLEMAKEIMAGE) {
        mVEHICLEMAKEIMAGE = vEHICLEMAKEIMAGE;
    }

}
