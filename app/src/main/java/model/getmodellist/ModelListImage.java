
package model.getmodellist;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ModelListImage {

    @SerializedName("VEHICLE_MODEL")
    private String mVEHICLEMODEL;
    @SerializedName("VEHICLE_MODEL_IMAGE")
    private String mVEHICLEMODELIMAGE;

    public String getVEHICLEMODEL() {
        return mVEHICLEMODEL;
    }

    public void setVEHICLEMODEL(String vEHICLEMODEL) {
        mVEHICLEMODEL = vEHICLEMODEL;
    }

    public String getVEHICLEMODELIMAGE() {
        return mVEHICLEMODELIMAGE;
    }

    public void setVEHICLEMODELIMAGE(String vEHICLEMODELIMAGE) {
        mVEHICLEMODELIMAGE = vEHICLEMODELIMAGE;
    }

}
