
package model.getchildpartsdetails;

import com.google.gson.annotations.SerializedName;

import java.util.List;


@SuppressWarnings("unused")
public class GetChildParts {

    @SerializedName("data")
    private List<GetChildPartData> mData;
    @SerializedName("result")
    private String mResult;

    public List<GetChildPartData> getData() {
        return mData;
    }

    public void setData(List<GetChildPartData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
