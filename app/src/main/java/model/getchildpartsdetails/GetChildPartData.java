
package model.getchildpartsdetails;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class GetChildPartData {

    @SerializedName("CHILD_PART_No")
    private String mCHILDPARTNo;
    @SerializedName("DESCRIPTION")
    private String mDESCRIPTION;
    @SerializedName("ILL_NO")
    private String mILLNO;
    @SerializedName("id")
    private String mId;
    @SerializedName("No_OFF")
    private String mNoOFF;
    @SerializedName("PARENT_PART_NO")
    private String mPARENTPARTNO;

    public String getCHILDPARTNo() {
        return mCHILDPARTNo;
    }

    public void setCHILDPARTNo(String cHILDPARTNo) {
        mCHILDPARTNo = cHILDPARTNo;
    }

    public String getDESCRIPTION() {
        return mDESCRIPTION;
    }

    public void setDESCRIPTION(String dESCRIPTION) {
        mDESCRIPTION = dESCRIPTION;
    }

    public String getILLNO() {
        return mILLNO;
    }

    public void setILLNO(String iLLNO) {
        mILLNO = iLLNO;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getNoOFF() {
        return mNoOFF;
    }

    public void setNoOFF(String noOFF) {
        mNoOFF = noOFF;
    }

    public String getPARENTPARTNO() {
        return mPARENTPARTNO;
    }

    public void setPARENTPARTNO(String pARENTPARTNO) {
        mPARENTPARTNO = pARENTPARTNO;
    }

}
