
package model.getvehiclemodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;


@SuppressWarnings("unused")
public class GetVehicleModelList {

    @SerializedName("data")
    private List<GetVehicleModelData> mData;
    @SerializedName("result")
    private String mResult;

    public List<GetVehicleModelData> getData() {
        return mData;
    }

    public void setData(List<GetVehicleModelData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
