package com.lucas.lucas2wheeler;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import APIInterface.CategoryAPI;
import RetroClient.RetroClient;
import model.partdetails.PartDetailsJson;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PartDetails extends AppCompatActivity {

    private TextView partno,product,type,voltage,rating,model,oecust,application,engine,status,
            headerflow,servicePartnooormake,headerpartnumber,modelorinvisi,mrppice,gst;
    private String partnumber,flow="";
    private AlertDialog alerterror;
    private LinearLayout flowheader,oemlayout,modellayoutorvehiclename,partnumberlayout,partnumberl,
            productlayout,typelayout,voltagelayout,outputrating,modellayout,
            oecustomer,applicationlayout,enginelayout,statuslayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_part_details);

        flow=getIntent().getStringExtra("flow");
        partnumber=getIntent().getStringExtra("partnumber");

        headerflow=findViewById(R.id.headerflow);
        servicePartnooormake=findViewById(R.id.servicePartnooormake);
        headerpartnumber=findViewById(R.id.headerpartnumber);

        modelorinvisi=findViewById(R.id.modelorinvisi);
        flowheader=findViewById(R.id.flowheader);
        oemlayout=findViewById(R.id.oemlayout);
        modellayoutorvehiclename=findViewById(R.id.modellayoutorvehiclename);
        partnumberlayout=findViewById(R.id.partnumberlayout);

        partnumberl=findViewById(R.id.partnumberl);
        productlayout=findViewById(R.id.productlayout);
        typelayout=findViewById(R.id.typelayout);
        voltagelayout=findViewById(R.id.voltagelayout);
        outputrating=findViewById(R.id.outputrating);
        modellayout=findViewById(R.id.modellayout);
        oecustomer=findViewById(R.id.oecustomer);
        applicationlayout=findViewById(R.id.applicationlayout);
        enginelayout=findViewById(R.id.enginelayout);
        statuslayout=findViewById(R.id.statuslayout);
        mrppice=findViewById(R.id.mrppice);
        gst=findViewById(R.id.gst);

        partno=findViewById(R.id.partno);
        product=findViewById(R.id.product);
        type=findViewById(R.id.type);
        voltage=findViewById(R.id.voltage);
        rating=findViewById(R.id.rating);
        model=findViewById(R.id.model);
        oecust=findViewById(R.id.oecust);
        application=findViewById(R.id.application);
        engine=findViewById(R.id.engine);
        status=findViewById(R.id.status);


        if (flow.equals("partnumber")||flow.equals("vehiclemodel")){
            modellayoutorvehiclename.setVisibility(View.GONE);
            oemlayout.setVisibility(View.GONE);
            headerpartnumber.setText(getIntent().getStringExtra("partnumber"));
        }
        else if (flow.equals("serviceparts")) {
            modellayoutorvehiclename.setVisibility(View.GONE);
            headerpartnumber.setText(getIntent().getStringExtra("partnumber"));
            servicePartnooormake.setText(getIntent().getStringExtra("servicepartnumberoroem"));
        }
        headerflow.setText(getIntent().getStringExtra("header"));
        servicePartnooormake.setText(getIntent().getStringExtra("servicepartnumberoroem"));
        headerpartnumber.setText(getIntent().getStringExtra("partnumber"));

        if (getIntent().getStringExtra("selectedmodel").equals("")){
            modelorinvisi.setVisibility(View.GONE);
        }
        else {
            modelorinvisi.setText(getIntent().getStringExtra("selectedmodel"));
        }

        if (flow.equals("makemodel")){
            partnumberlayout.setVisibility(View.GONE);
            String oem,model;
            oem=getIntent().getStringExtra("oem");
            model=getIntent().getStringExtra("model");
            getoemmodeldetails(oem,model);
        }
        else if (flow.equals("vehiclemodel")||flow.equals("makemodel")){
            getVehicleMakeDetails();
        }
        else {
            getpartdetails();
        }
    }

    private void getVehicleMakeDetails() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(PartDetails.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiService();
            // Calling JSON

            Call<PartDetailsJson> call = service.getVehicleModelDetails(partnumber);
            call.enqueue(new Callback<PartDetailsJson>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(Call<PartDetailsJson> call, Response<PartDetailsJson> response) {
                    if (response.body().getResult().equals("success")) {
                        progressDialog.dismiss();
                        if (TextUtils.isEmpty(response.body().getData().getPARTNUMBER())){
                            partnumberl.setVisibility(View.GONE);
                        } if (TextUtils.isEmpty(response.body().getData().getDESCRIPTION())){
                            productlayout.setVisibility(View.GONE);
                        } if (TextUtils.isEmpty(response.body().getData().getTYPE())){
                            typelayout.setVisibility(View.GONE);
                        } if (TextUtils.isEmpty(response.body().getData().getVOLT())){
                            voltagelayout.setVisibility(View.GONE);
                        } if (TextUtils.isEmpty(response.body().getData().getRATING())){
                            outputrating.setVisibility(View.GONE);
                        } if (TextUtils.isEmpty(response.body().getData().getVEHICLEMODEL())){
                            modellayout.setVisibility(View.GONE);
                        } if (TextUtils.isEmpty(response.body().getData().getVEHICLEMAKE())){
                            oecustomer.setVisibility(View.GONE);
                        } if (TextUtils.isEmpty(response.body().getData().getVEHICLEMODEL())){
                            applicationlayout.setVisibility(View.GONE);
                        }
                        partno.setText(response.body().getData().getPARTNUMBER());
                        product.setText(response.body().getData().getDESCRIPTION());
                        type.setText(response.body().getData().getTYPE());
                        voltage.setText(response.body().getData().getVOLT());
                        rating.setText(response.body().getData().getRATING());
                        model.setText(response.body().getData().getVEHICLEMODEL());
                        oecust.setText(response.body().getData().getVEHICLEMAKE());
                        application.setText(response.body().getData().getVEHICLEMODEL());
                        mrppice.setText(response.body().getData().getMRP());
                        gst.setText(response.body().getData().getmGST()+"%"+"");

                        if (response.body().getData().getmSTATUS().equals("ACTIVE")){
                            status.setText(response.body().getData().getmSTATUS());
                            status.setTextColor(getResources().getColor(R.color.blue));
                        }else if (response.body().getData().getmSTATUS().equals("INACTIVE")){
                            status.setText(response.body().getData().getmSTATUS());
                            status.setTextColor(getResources().getColor(R.color.red));
                        }
                        //if status is obsolete navigate the part number in obsolete partnumber show the details
                    } else {
                        progressDialog.dismiss();
                        getfailurealert("Please Try Again Later");
                    }
                }
                @Override
                public void onFailure(Call<PartDetailsJson> call, Throwable t) {
                    progressDialog.dismiss();
                    getfailurealert("Failed to fetch data from server");
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
            getfailurealert("Exception error please try again later");
        }
    }


    private void getoemmodeldetails(String oem, String modelpost) {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(PartDetails.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiService();
            // Calling JSON
            Call<PartDetailsJson> call = service.getPartDetails(oem,modelpost);
            call.enqueue(new Callback<PartDetailsJson>() {
                @Override
                public void onResponse(Call<PartDetailsJson> call, Response<PartDetailsJson> response) {
                    if (response.body().getResult().equals("success")) {
                        progressDialog.dismiss();
                        if (TextUtils.isEmpty(response.body().getData().getPARTNUMBER())){
                            partnumberl.setVisibility(View.GONE);
                        } if (TextUtils.isEmpty(response.body().getData().getDESCRIPTION())){
                            productlayout.setVisibility(View.GONE);
                        } if (TextUtils.isEmpty(response.body().getData().getTYPE())){
                            typelayout.setVisibility(View.GONE);
                        } if (TextUtils.isEmpty(response.body().getData().getVOLT())){
                            voltagelayout.setVisibility(View.GONE);
                        } if (TextUtils.isEmpty(response.body().getData().getRATING())){
                            outputrating.setVisibility(View.GONE);
                        } if (TextUtils.isEmpty(response.body().getData().getVEHICLEMODEL())){
                            modellayout.setVisibility(View.GONE);
                        } if (TextUtils.isEmpty(response.body().getData().getVEHICLEMAKE())){
                            oecustomer.setVisibility(View.GONE);
                        } if (TextUtils.isEmpty(response.body().getData().getVEHICLEMODEL())){
                            applicationlayout.setVisibility(View.GONE);
                        }
                        partno.setText(response.body().getData().getPARTNUMBER());
                        product.setText(response.body().getData().getDESCRIPTION());
                        type.setText(response.body().getData().getTYPE());
                        voltage.setText(response.body().getData().getVOLT());
                        rating.setText(response.body().getData().getRATING());
                        model.setText(response.body().getData().getVEHICLEMODEL());
                        oecust.setText(response.body().getData().getVEHICLEMAKE());
                        application.setText(response.body().getData().getVEHICLEMODEL());
                        mrppice.setText(response.body().getData().getMRP());
                        gst.setText(response.body().getData().getmGST()+"%"+"");
                        if (response.body().getData().getmSTATUS().equals("ACTIVE")){
                            status.setText(response.body().getData().getmSTATUS());
                            status.setTextColor(getResources().getColor(R.color.blue));
                        }else if (response.body().getData().getmSTATUS().equals("INACTIVE")){
                            status.setText(response.body().getData().getmSTATUS());
                            status.setTextColor(getResources().getColor(R.color.red));
                        }
                    } else {
                        progressDialog.dismiss();
                        getfailurealert("Please Try Again Later");
                    }
                }
                @Override
                public void onFailure(Call<PartDetailsJson> call, Throwable t) {
                    progressDialog.dismiss();
                    getfailurealert("Failed to fetch data from server");
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
            getfailurealert("Exception error please try again later");
        }
    }

    private void getpartdetails() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(PartDetails.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiService();
            // Calling JSON

            Call<PartDetailsJson> call = service.getPartDetails(partnumber);
            call.enqueue(new Callback<PartDetailsJson>() {
                @Override
                public void onResponse(Call<PartDetailsJson> call, Response<PartDetailsJson> response) {
                    if (response.body().getResult().equals("success")) {
                        progressDialog.dismiss();
                        if (TextUtils.isEmpty(response.body().getData().getPARTNUMBER())){
                            partnumberl.setVisibility(View.GONE);
                        } if (TextUtils.isEmpty(response.body().getData().getDESCRIPTION())){
                            productlayout.setVisibility(View.GONE);
                        } if (TextUtils.isEmpty(response.body().getData().getTYPE())){
                            typelayout.setVisibility(View.GONE);
                        } if (TextUtils.isEmpty(response.body().getData().getVOLT())){
                            voltagelayout.setVisibility(View.GONE);
                        } if (TextUtils.isEmpty(response.body().getData().getRATING())){
                            outputrating.setVisibility(View.GONE);
                        } if (TextUtils.isEmpty(response.body().getData().getVEHICLEMODEL())){
                            modellayout.setVisibility(View.GONE);
                        } if (TextUtils.isEmpty(response.body().getData().getVEHICLEMAKE())){
                            oecustomer.setVisibility(View.GONE);
                        } if (TextUtils.isEmpty(response.body().getData().getVEHICLEMODEL())){
                            applicationlayout.setVisibility(View.GONE);
                        }
                        partno.setText(response.body().getData().getPARTNUMBER());
                        product.setText(response.body().getData().getDESCRIPTION());
                        type.setText(response.body().getData().getTYPE());
                        voltage.setText(response.body().getData().getVOLT());
                        rating.setText(response.body().getData().getRATING());
                        model.setText(response.body().getData().getVEHICLEMODEL());
                        oecust.setText(response.body().getData().getVEHICLEMAKE());
                        application.setText(response.body().getData().getVEHICLEMODEL());
                        mrppice.setText(response.body().getData().getMRP());
                        gst.setText(response.body().getData().getmGST()+"%"+"");
                        if (response.body().getData().getmSTATUS().equals("ACTIVE")){
                            status.setText(response.body().getData().getmSTATUS());
                            status.setTextColor(getResources().getColor(R.color.blue));
                        }else if (response.body().getData().getmSTATUS().equals("INACTIVE")){
                            status.setText(response.body().getData().getmSTATUS());
                            status.setTextColor(getResources().getColor(R.color.red));
                        }
                    } else {
                        progressDialog.dismiss();
                        getfailurealert("Please Try Again Later");
                    }
                }
                @Override
                public void onFailure(Call<PartDetailsJson> call, Throwable t) {
                    progressDialog.dismiss();
                    getfailurealert("Failed to fetch data from server");
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
            getfailurealert("Exception error please try again later");
        }
    }

    private void getfailurealert(String alert) {
        alerterror=new AlertDialog.Builder(PartDetails.this).create();
        View orderplacedshow= LayoutInflater.from(PartDetails.this).inflate(R.layout.alertbox,null);
        TextView alerttext=orderplacedshow.findViewById(R.id.alerttext);
        TextView alertok=orderplacedshow.findViewById(R.id.alertok);
        alerttext.setText(alert);

        alertok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });
        alerterror.setView(orderplacedshow);
        alerterror.setCanceledOnTouchOutside(false);
        alerterror.setCancelable(false);
        alerterror.show();
    }

    public void Home(View view) {
        startActivity(new Intent(this, MainActivity.class));
    }

    public void Back(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
