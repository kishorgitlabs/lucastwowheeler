package com.lucas.lucas2wheeler;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import adapter.VehicleModelRecyclerAdapter;


public class VehicleMake extends AppCompatActivity implements VehicleModelRecyclerAdapter.ActivityChanger {


    private ExpandableListView vehiclemodellist;
    private RecyclerView oemlist;
    private VehicleModelFragement vehicleModel;
    private static final int CONTENT_VIEW_ID = 10101010;
    private FrameLayout fragemnt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_make);

        oemlist=findViewById(R.id.oemlist);
        fragemnt=findViewById(R.id.fragemnt);

        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        VehicleMakeFragement vehicleMakeFragement = new VehicleMakeFragement();
        fragmentTransaction.add(R.id.fragemnt, vehicleMakeFragement, "HELLO");
        fragmentManager.beginTransaction();
        fragmentTransaction.commit();
    }

    @Override
    public void intent(String oem, String model) {
        Intent partdetail=new Intent(getApplicationContext(),PartDetails.class);
        partdetail.putExtra("oem",oem);
        partdetail.putExtra("model",model);
        partdetail.putExtra("header","Vehicle Make");
        partdetail.putExtra("servicepartnumberoroem",oem);
        partdetail.putExtra("selectedmodel",model);
        partdetail.putExtra("partnumber",model);
        partdetail.putExtra("flow","makemodel");
        startActivity(partdetail);
    }

    public void Home(View view) {
        startActivity(new Intent(this, MainActivity.class));
    }

    public void Back(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
