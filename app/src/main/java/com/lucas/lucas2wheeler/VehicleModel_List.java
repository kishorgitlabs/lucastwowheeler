package com.lucas.lucas2wheeler;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import APIInterface.CategoryAPI;
import RetroClient.RetroClient;
import adapter.ClearableAutoCompleteTextView;
import adapter.VehicelModelAdapter;
import model.getmodellist.GetModelList;
import model.getmodellist.ModelListImage;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VehicleModel_List extends AppCompatActivity {


    private RecyclerView vehiclemodellist;
    private List<ModelListImage> vehiclemodelimage;
    private ClearableAutoCompleteTextView vehicleautocompletesearch;
    private AlertDialog alerterror;
    private VehicelModelAdapter vehicelModelAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_model__list);

        vehiclemodellist=findViewById(R.id.vehiclemodellist);
        vehicleautocompletesearch=findViewById(R.id.vehicleautocompletesearch);
        vehicleautocompletesearch.hideClearButton();


        vehicleautocompletesearch.setOnClearListener(new ClearableAutoCompleteTextView.OnClearListener() {
            @Override
            public void onClear() {
                vehicleautocompletesearch.setText("");
            }
        });

        vehicleautocompletesearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void afterTextChanged(Editable s) {
                String searched=s.toString().toLowerCase();
                if (searched.length()>0){
                    List<ModelListImage>  vehiclemodellist=new ArrayList<>();
                    vehicleautocompletesearch.showClearButton();

                    for (ModelListImage vehiclemode:vehiclemodelimage)
                    {
                    if (vehiclemode.getVEHICLEMODEL().toLowerCase().contains(searched)){
                        vehiclemodellist.add(vehiclemode);

                    }
                    }
                    if (vehicelModelAdapter!=null){
                        vehicelModelAdapter.setModelList(vehiclemodellist);
                        vehicelModelAdapter.notifyDataSetChanged();
                    }
                }else {
                    vehicleautocompletesearch.hideClearButton();
                    if (vehicelModelAdapter!=null) {
                        vehicelModelAdapter.setModelList(vehiclemodelimage);
                        vehicelModelAdapter.notifyDataSetChanged();
                    }
                }
            }
        });
        CheckInternet();
    }
    private void CheckInternet() {
        NetworkConnection networkConnection=new NetworkConnection(VehicleModel_List.this);
        if (networkConnection.CheckInternet()){
            getVehicleModel();
        }else {
            getfailurealert("Please Turn On Mobile Data Or Connect To Wifi");
        }
    }

    private void getVehicleModel() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(VehicleModel_List.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiService();
            // Calling JSON
            Call<GetModelList> call = service.getVehicleModellist();
            call.enqueue(new Callback<GetModelList>() {
                @Override
                public void onResponse(Call<GetModelList> call, Response<GetModelList> response) {
                    if (response.body().getResult().equals("success")) {
                        progressDialog.dismiss();
                        vehiclemodelimage=new ArrayList<>();
                        vehiclemodelimage=response.body().getData();
                         vehicelModelAdapter=new VehicelModelAdapter(VehicleModel_List.this,vehiclemodelimage);
                        vehiclemodellist.setAdapter(vehicelModelAdapter);
                        vehiclemodellist.setLayoutManager(new LinearLayoutManager(VehicleModel_List.this));
                    } else {
                        progressDialog.dismiss();
                        getfailurealert("Please Try Again Later");
                    }
                }
                @Override
                public void onFailure(Call<GetModelList> call, Throwable t) {
                    progressDialog.dismiss();
                    getfailurealert("Failed to fetch data from server");
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
            getfailurealert("Exception error please try again later");
        }
    }


    private void getfailurealert(String alert) {
        alerterror=new AlertDialog.Builder(VehicleModel_List.this).create();
        View orderplacedshow= LayoutInflater.from(VehicleModel_List.this).inflate(R.layout.alertbox,null);
        TextView alerttext=orderplacedshow.findViewById(R.id.alerttext);
        TextView alertok=orderplacedshow.findViewById(R.id.alertok);
        alerttext.setText(alert);

        alertok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });
        alerterror.setView(orderplacedshow);
        alerterror.setCanceledOnTouchOutside(false);
        alerterror.show();
    }

    public void Home(View view) {
        startActivity(new Intent(this, MainActivity.class));
    }

    public void Back(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
