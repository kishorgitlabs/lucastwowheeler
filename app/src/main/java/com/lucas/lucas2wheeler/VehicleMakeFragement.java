package com.lucas.lucas2wheeler;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import APIInterface.CategoryAPI;
import RetroClient.RetroClient;
import adapter.GridSpacingItemDecoration;
import adapter.VehicleOemlRecyclerAdapter;
import model.getOemList.GetOemData;
import model.getOemList.GetOemList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VehicleMakeFragement extends Fragment {

    private List<GetOemData> oem;
    private AlertDialog alerterror;
    private RecyclerView oemlist;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.vehiclemake,container,false);
         oemlist=view.findViewById(R.id.oemlist);
        TextView fragementHeader=view.findViewById(R.id.fragementHeader);
        fragementHeader.setText("OEM");

        CheckInternet();
        return view;
    }

    private void CheckInternet() {
        NetworkConnection networkConnection=new NetworkConnection(getActivity());
        if (networkConnection.CheckInternet()){
            getVehicleMake();
        }else {
            getfailurealert("Please Turn On Mobile Data Or Connect To Wifi");
        }
    }

    private void getVehicleMake() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(getActivity(),
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiService();
            // Calling JSON
            Call<GetOemList> call = service.getOemList();
            call.enqueue(new Callback<GetOemList>() {
                @Override
                public void onResponse(Call<GetOemList> call, Response<GetOemList> response) {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("success")) {
                        oem=response.body().getData();
                        VehicleOemlRecyclerAdapter vehicleOemlRecyclerAdapter =new VehicleOemlRecyclerAdapter(getActivity(),oem);
                        oemlist.addItemDecoration(new GridSpacingItemDecoration(4, dpToPx(10), true));
                        oemlist.setAdapter(vehicleOemlRecyclerAdapter);
                        LinearLayoutManager linearLayoutManager=new GridLayoutManager(getActivity(),4);
                        oemlist.setItemAnimator(new DefaultItemAnimator());
                        oemlist.setLayoutManager(linearLayoutManager);
                    } else {
                        getfailurealert("Please Try Again Later");
                    }
                }
                @Override
                public void onFailure(Call<GetOemList> call, Throwable t) {
                    progressDialog.dismiss();
                    getfailurealert("Failed to fetch data from server");
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
            getfailurealert("Exception error please try again later");
        }
    }

    private void getfailurealert(String alert) {
        alerterror=new AlertDialog.Builder(getActivity()).create();
        View orderplacedshow= LayoutInflater.from(getActivity()).inflate(R.layout.alertbox,null);
        TextView alerttext=orderplacedshow.findViewById(R.id.alerttext);
        TextView alertok=orderplacedshow.findViewById(R.id.alertok);
        alerttext.setText(alert);

        alertok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        alerterror.setView(orderplacedshow);
        alerterror.setCanceledOnTouchOutside(false);
        alerterror.show();
    }
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
