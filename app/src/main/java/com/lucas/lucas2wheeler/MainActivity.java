package com.lucas.lucas2wheeler;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.muddzdev.styleabletoastlibrary.StyleableToast;

import java.util.List;

import APIInterface.CategoryAPI;
import RetroClient.RetroClient;
import model.getpartnolist.GetPartNo;
import model.getservicepartnolist.GetServicePartNo;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private List<String> partno;
    private List<String> servicepartno;
    private LinearLayout partnosearch,servicespart,vehiclemodel,vehiclemakes,sendenquiry,pricelist;
    private AlertDialog partnumbersearch;
    private AlertDialog alerterror;
    private ImageView setting;
    private AlertDialog alertDialogpartnumber;
    private AlertDialog alertDialogservicepartnumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        partnosearch=findViewById(R.id.partnosearch);
        servicespart=findViewById(R.id.servicespart);
        vehiclemodel=findViewById(R.id.vehiclemodel);
        vehiclemakes=findViewById(R.id.vehiclemakes);
        sendenquiry=findViewById(R.id.sendenquiry);
        pricelist=findViewById(R.id.pricelist);
        setting=findViewById(R.id.setting);

        setting.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(MainActivity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);

                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_exit:
                                finish();
                                break;
                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.settingmenu);
                try {
                    PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                    int version = pInfo.versionCode;
                    MenuItem appVersion=pop.getMenu().findItem(R.id.version);
                    appVersion.setTitle("App Version : "+version+".0"+"");
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                pop.show();
            }
        });


        partnosearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckInternetPartno();
            }
        });

        servicespart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckInternet();
            }
        });

        vehiclemodel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent vehiclemake=new Intent(getApplicationContext(),VehicleModel_List.class);
                startActivity(vehiclemake);
//                getVehicleModel();

            }
        });

        vehiclemakes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent vehicle=new Intent(getApplicationContext(),VehicleMake.class);
                startActivity(vehicle);
            }
        });
        sendenquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent enquiry=new Intent(getApplicationContext(),SendEnquiry.class);
                startActivity(enquiry);
            }
        });

        pricelist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent contactus=new Intent(getApplicationContext(),PriceList.class);
                startActivity(contactus);
            }
        });
    }

    private void CheckInternet() {
        NetworkConnection networkConnection=new NetworkConnection(MainActivity.this);
        if (networkConnection.CheckInternet()){
            getServiceParts();
        }else {
            getfailurealert("Please Turn On Mobile Data Or Connect To Wifi");
        }
    }
    private void CheckInternetPartno() {
        NetworkConnection networkConnection=new NetworkConnection(MainActivity.this);
        if (networkConnection.CheckInternet()){
            getPartNumber();
        }else {
            getfailurealert("Please Turn On Mobile Data Or Connect To Wifi");
        }
    }


    private void getServiceParts() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(MainActivity.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiService();
            // Calling JSON
            Call<GetServicePartNo> call = service.getServiceParts();
            call.enqueue(new Callback<GetServicePartNo>() {
                @Override
                public void onResponse(Call<GetServicePartNo> call, Response<GetServicePartNo> response) {
                    if (response.body().getResult().equals("success")) {
                        progressDialog.dismiss();
                        servicepartno=response.body().getData();
                        searchAlertServiceParts(servicepartno);
                    } else {
                        progressDialog.dismiss();
                        getfailurealert("Please Try Again Later");
                    }
                }
                @Override
                public void onFailure(Call<GetServicePartNo> call, Throwable t) {
                    progressDialog.dismiss();
                    getfailurealert("Failed to fetch data from server");
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
            getfailurealert("Exception error please try again later");
        }
    }
    private void getPartNumber() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(MainActivity.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiService();
            // Calling JSON
            Call<GetPartNo> call = service.getPartNo();
            call.enqueue(new Callback<GetPartNo>() {
                @Override
                public void onResponse(Call<GetPartNo> call, Response<GetPartNo> response) {
                    if (response.body().getResult().equals("success")) {
                        progressDialog.dismiss();
                        partno=response.body().getData();
                        searchAlert(partno);
                    } else {
                        progressDialog.dismiss();
                        getfailurealert("Please Try Again Later");
                    }
                }
                @Override
                public void onFailure(Call<GetPartNo> call, Throwable t) {
                    progressDialog.dismiss();
                    getfailurealert("Failed to fetch data from server");
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
            getfailurealert("Exception error please try again later");
        }
    }



    public void searchAlert(List<String> partno){

        alertDialogpartnumber = new AlertDialog.Builder(MainActivity.this).create();
        LayoutInflater inflater = (MainActivity.this).getLayoutInflater();
        View searchAlert = inflater.inflate(R.layout.partalert, null);
        TextView titletext=searchAlert.findViewById(R.id.titletext);
        final AutoCompleteTextView partnoauto=searchAlert.findViewById(R.id.autocomplete);
        Button back=searchAlert.findViewById(R.id.back);
        Button search=searchAlert.findViewById(R.id.search);

        titletext.setText("Please enter part number!");
        partnoauto.setHint("Enter Part Number");
//        AutoSuggestAdapter partnumberdapter = new AutoSuggestAdapter(MainActivity.this, android.R.layout.simple_dropdown_item_1line, partno);
//        partnoauto.setAdapter(partnumberdapter);
//        // specify the minimum type of characters before drop-down list is shown
//        partnoauto.setThreshold(1);
        partnoauto.setAdapter(new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_list_item_1,partno));
        partnoauto.setThreshold(1);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String selectedAuto=partnoauto.getText().toString();
                if (TextUtils.isEmpty(selectedAuto)){
                    StyleableToast st = new StyleableToast(getApplication(),
                            "Please enter partnumber", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplication().getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }
                else {
                    alertDialogpartnumber.dismiss();
                    Intent partdetails=new Intent(MainActivity.this, PartDetails.class);
                    partdetails.putExtra("partnumber", selectedAuto);
                    partdetails.putExtra("flow","partnumber");
                    partdetails.putExtra("header","Part Search");
                    partdetails.putExtra("selectedmodel","");
                    startActivity(partdetails);
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogpartnumber.dismiss();
            }
        });
        alertDialogpartnumber.setView(searchAlert);
        alertDialogpartnumber.show();
        alertDialogpartnumber.setCanceledOnTouchOutside(false);
    }


    public void searchAlertServiceParts(List<String> servicepartno) {

        alertDialogservicepartnumber = new AlertDialog.Builder(MainActivity.this).create();
        LayoutInflater inflater = (MainActivity.this).getLayoutInflater();
        View searchAlert = inflater.inflate(R.layout.partalert, null);

        TextView titletext=searchAlert.findViewById(R.id.titletext);
        final AutoCompleteTextView serviceautocomplete=searchAlert.findViewById(R.id.autocomplete);
        Button back=searchAlert.findViewById(R.id.back);
        Button search=searchAlert.findViewById(R.id.search);
        titletext.setText("Please enter service part number!");
        serviceautocomplete.setHint("Enter service Partnumber");
//        AutoSuggestAdapter servicepartadapter = new AutoSuggestAdapter(MainActivity.this, android.R.layout.simple_dropdown_item_1line, servicepartno);
//        serviceautocomplete.setAdapter(servicepartadapter);
//        // specify the minimum type of characters before drop-down list is shown
//        serviceautocomplete.setThreshold(1);
        serviceautocomplete.setAdapter(new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_list_item_1,servicepartno));
        serviceautocomplete.setThreshold(1);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String selectedAuto=serviceautocomplete.getText().toString();
                if (TextUtils.isEmpty(selectedAuto)){
                    StyleableToast st = new StyleableToast(getApplication(),
                            "Please enter service partnumber", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplication().getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else {
                    alertDialogservicepartnumber.dismiss();
                    Intent partdetails=new Intent(MainActivity.this, Service_Parts.class);
                    partdetails.putExtra("servicepartsnumber", selectedAuto);
                    partdetails.putExtra("flow","partnumber");
                    partdetails.putExtra("header","Part Search");
                    partdetails.putExtra("selectedmodel","");
                    startActivity(partdetails);
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogservicepartnumber.dismiss();
            }
        });
        alertDialogservicepartnumber.setView(searchAlert);
        alertDialogservicepartnumber.show();
        alertDialogservicepartnumber.setCanceledOnTouchOutside(false);
    }



    private void getfailurealert(String alert) {
        alerterror=new AlertDialog.Builder(MainActivity.this).create();
        View orderplacedshow= LayoutInflater.from(MainActivity.this).inflate(R.layout.alertbox,null);
        TextView alerttext=orderplacedshow.findViewById(R.id.alerttext);
        TextView alertok=orderplacedshow.findViewById(R.id.alertok);
        alerttext.setText(alert);

        alertok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alerterror.dismiss();
            }
        });
        alerterror.setView(orderplacedshow);
        alerterror.setCanceledOnTouchOutside(false);
        alerterror.show();
    }

}
