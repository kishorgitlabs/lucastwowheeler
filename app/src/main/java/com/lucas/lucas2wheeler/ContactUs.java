package com.lucas.lucas2wheeler;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.muddzdev.styleabletoastlibrary.StyleableToast;

import java.util.List;

import APIInterface.CategoryAPI;
import RetroClient.RetroClient;
import model.getcityforserviceengg.GetServiceEnggCity;
import model.serviceengineer.GetServiceEngineer;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactUs extends AppCompatActivity {


    private ArrayAdapter<CharSequence> adapter;
    private String stateselected,cityselected;
    private AlertDialog alerterror;
    private List<String> citylist;
    private TextView engineername,mobile,emailid,location;
    private LinearLayout servicedetails;
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        servicedetails=findViewById(R.id.servicedetails);

         engineername=findViewById(R.id.engineername);
         mobile=findViewById(R.id.mobile);
         emailid=findViewById(R.id.emailid);
         location=findViewById(R.id.location);

        CheckInternet();
    }
    private void CheckInternet() {
        NetworkConnection networkConnection=new NetworkConnection(ContactUs.this);
        if (networkConnection.CheckInternet()){
            getstatecityalert();
        }else {
            getfailurealert("Please Turn On Mobile Data Or Connect To Wifi");
        }
    }

    private void getstatecityalert() {

        alertDialog = new android.app.AlertDialog.Builder(
                ContactUs.this).create();
        LayoutInflater inflater = (ContactUs.this).getLayoutInflater();
        final View dialog = inflater.inflate(R.layout.statecity, null);

        alertDialog.setView(dialog);
        Spinner statespin=dialog.findViewById(R.id.statespin);
        Button search=dialog.findViewById(R.id.searchengg);
        Button cancel=dialog.findViewById(R.id.cancel);
        final Spinner cityspin=dialog.findViewById(R.id.cityspin);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stateselected.equals("Select State")){
                    StyleableToast st = new StyleableToast(getApplication(),
                            "Select State", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplication().getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else if (cityselected.equals("Select City")){
                    StyleableToast st = new StyleableToast(getApplication(),
                            "Select City", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplication().getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else {
                    alertDialog.dismiss();
                    getServiceEngineerDetails();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        statespin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                stateselected=adapterView.getSelectedItem().toString();
                if (!stateselected.equals("Select State")){
                    StyleableToast st = new StyleableToast(getApplication(),
                            "Select City", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplication().getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();

                    getserviceenggcity();
                }
                else {
                    StyleableToast st = new StyleableToast(getApplication(),
                            "Select State", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplication().getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }
            }
            private void getserviceenggcity() {
                try {
                    final ProgressDialog progressDialog = new ProgressDialog(ContactUs.this,
                            R.style.Progress);
                    progressDialog.setIndeterminate(true);
                    progressDialog.setMessage("Loading...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    CategoryAPI service = RetroClient.getApiService();
                    // Calling JSON
                    Call<GetServiceEnggCity> call = service.getServiceEnggCity(stateselected);
                    call.enqueue(new Callback<GetServiceEnggCity>() {
                        @Override
                        public void onResponse(Call<GetServiceEnggCity> call, Response<GetServiceEnggCity> response) {
                            progressDialog.dismiss();
                            if (response.body().getResult().equals("success")) {
                                progressDialog.dismiss();
                                citylist=response.body().getData();
                                citylist.add(0, "Select City");
                                ArrayAdapter adapter=new ArrayAdapter(getApplicationContext(),android.R.layout.simple_spinner_item,citylist);
                                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                                cityspin.setAdapter(adapter);

                            } else {
                                progressDialog.dismiss();
                                servicedetails.setVisibility(View.GONE);
                                getfailurealert("No Engineers in selected state please select another state");
                            }
                        }
                        @Override
                        public void onFailure(Call<GetServiceEnggCity> call, Throwable t) {
                            progressDialog.dismiss();
                            servicedetails.setVisibility(View.GONE);
                            getfailurealert("Failed to fetch data from server");
                        }
                    });
                } catch (Exception ex) {
                    ex.getMessage();
                    servicedetails.setVisibility(View.GONE);
                    getfailurealert("Exception error please try again later");
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        cityspin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                cityselected=adapterView.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        adapter = ArrayAdapter.createFromResource(this, R.array.state_array, R.layout.support_simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        statespin.setAdapter(adapter);

        alertDialog.show();
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
    }

    private void getServiceEngineerDetails() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(ContactUs.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiService();
            // Calling JSON
            Call<GetServiceEngineer> call = service.getServiceEngineerDetails(stateselected,cityselected);
            call.enqueue(new Callback<GetServiceEngineer>() {
                @Override
                public void onResponse(Call<GetServiceEngineer> call, Response<GetServiceEngineer> response) {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("success")) {
                        progressDialog.dismiss();
                        servicedetails.setVisibility(View.VISIBLE);
                        engineername.setText(response.body().getData().getName());
                        mobile.setText(response.body().getData().getMobile());
                        emailid.setText(response.body().getData().getEmail());
                        location.setText(response.body().getData().getLocation());
                    } else {
                        progressDialog.dismiss();
                        servicedetails.setVisibility(View.GONE);
                        getfailurealert("No Engineers in selected state");
                    }
                }
                @Override
                public void onFailure(Call<GetServiceEngineer> call, Throwable t) {
                    progressDialog.dismiss();
                    servicedetails.setVisibility(View.GONE);
                    getfailurealert("Failed to fetch data from server");
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
            servicedetails.setVisibility(View.GONE);
            getfailurealert("Exception error please try again later");
        }
    }

    private void getfailurealert(String alert) {
        alerterror=new AlertDialog.Builder(ContactUs.this).create();
        View orderplacedshow= LayoutInflater.from(ContactUs.this).inflate(R.layout.alertbox,null);
        TextView alerttext=orderplacedshow.findViewById(R.id.alerttext);
        TextView alertok=orderplacedshow.findViewById(R.id.alertok);
        alerttext.setText(alert);

        alertok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        alerterror.setView(orderplacedshow);
        alerterror.setCanceledOnTouchOutside(false);
        alerterror.show();
    }
    public void Home(View view) {
        startActivity(new Intent(this, MainActivity.class));
    }

    public void Back(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
