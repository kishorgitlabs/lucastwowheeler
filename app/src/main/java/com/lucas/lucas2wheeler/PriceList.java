package com.lucas.lucas2wheeler;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import APIInterface.CategoryAPI;
import RetroClient.RetroClient;
import adapter.PriceListAdapter;
import model.pricelist.GetPriceList;
import model.pricelist.PriceLlistData;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PriceList extends AppCompatActivity {

    private RecyclerView pricelist;
    private AlertDialog alerterror;
    private List<PriceLlistData> priceLlistData;
    private SearchView searchView;
    private PriceListAdapter priceListAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price_list);

        pricelist=findViewById(R.id.pricelist);
        searchView=findViewById(R.id.searchView);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (s.length()!=0){
                    priceListAdapter.filter(s);
                    priceListAdapter.notifyDataSetChanged();
                }else {
                    priceListAdapter.filter(s);
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    priceListAdapter.notifyDataSetChanged();
                }
                return false;
            }
        });
        CheckInternet();
    }

    private void CheckInternet() {
        NetworkConnection networkConnection=new NetworkConnection(PriceList.this);
        if (networkConnection.CheckInternet()){
            getPrice();
        }else {
            getfailurealert("Please Turn On Mobile Data Or Connect To Wifi");
        }
    }
    private void getPrice() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(PriceList.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiService();
            // Calling JSON
            Call<GetPriceList> call = service.getPriceList();
            call.enqueue(new Callback<GetPriceList>() {
                @Override
                public void onResponse(Call<GetPriceList> call, Response<GetPriceList> response) {
                    if (response.body().getResult().equals("success")) {
                        progressDialog.dismiss();
                        priceLlistData=response.body().getData();
                        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(PriceList.this);
                        pricelist.setLayoutManager(layoutManager);
                        priceListAdapter=new PriceListAdapter(PriceList.this,priceLlistData);
                        pricelist.setAdapter(priceListAdapter);
                    } else {
                        progressDialog.dismiss();
                        getfailurealert("Please Try Again Later");
                    }
                }
                @Override
                public void onFailure(Call<GetPriceList> call, Throwable t) {
                    progressDialog.dismiss();
                    getfailurealert("Failed to fetch data from server");
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
            getfailurealert("Exception error please try again later");
        }
    }

    public void Home(View view) {
        startActivity(new Intent(this, MainActivity.class));
    }

    public void Back(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void getfailurealert(String alert) {
        alerterror=new AlertDialog.Builder(PriceList.this).create();
        View orderplacedshow= LayoutInflater.from(PriceList.this).inflate(R.layout.alertbox,null);
        TextView alerttext=orderplacedshow.findViewById(R.id.alerttext);
        TextView alertok=orderplacedshow.findViewById(R.id.alertok);
        alerttext.setText(alert);

        alertok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        alerterror.setView(orderplacedshow);
        alerterror.setCanceledOnTouchOutside(false);
        alerterror.show();
    }
}
