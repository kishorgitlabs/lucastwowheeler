package com.lucas.lucas2wheeler;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.muddzdev.styleabletoastlibrary.StyleableToast;

import APIInterface.CategoryAPI;
import RetroClient.RetroClient;
import model.enquiry.SendEnquiryJson;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendEnquiry extends AppCompatActivity {

    private EditText enquiryname,enquiryemail,enquirymobile,enquirycity,enquiryepartno,enquirymessage;
    private Button submit;
    private AlertDialog alerterror;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_enquiry);

        enquiryname=findViewById(R.id.enquiryname);
        enquiryemail=findViewById(R.id.enquiryemail);
        enquirymobile=findViewById(R.id.enquirymobile);
        enquirycity=findViewById(R.id.enquirycity);
        enquiryepartno=findViewById(R.id.enquiryepartno);
        enquirymessage=findViewById(R.id.enquirymessage);
        submit=findViewById(R.id.submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ename,eemail,emobile,ecity,epartno,emessage;
                ename=enquiryname.getText().toString();
                eemail=enquiryemail.getText().toString();
                emobile=enquirymobile.getText().toString();
                ecity=enquirycity.getText().toString();
                epartno=enquiryepartno.getText().toString();
                emessage=enquirymessage.getText().toString();

                if (TextUtils.isEmpty(ename)){
                    StyleableToast st = new StyleableToast(getApplication(),
                            "Please enter name", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplication().getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else if (TextUtils.isEmpty(eemail)){
                    StyleableToast st = new StyleableToast(getApplication(),
                            "Please enter email", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplication().getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else if (TextUtils.isEmpty(emobile)){
                    StyleableToast st = new StyleableToast(getApplication(),
                            "Please enter mobileno", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplication().getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else if (emobile.length()!=10){
                    StyleableToast st = new StyleableToast(getApplication(),
                            "Please enter valid mobileno", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplication().getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }
                else if (TextUtils.isEmpty(ecity)){
                    StyleableToast st = new StyleableToast(getApplication(),
                            "Please enter city", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplication().getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }
                else if (TextUtils.isEmpty(emessage)) {
                    StyleableToast st = new StyleableToast(getApplication(),
                            "Message cannot be empty", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplication().getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }
                else {
                    sendEnquiry(ename,eemail,emobile,ecity,epartno,emessage);
                }
            }
        });

        CheckInternet();
    }

    private void CheckInternet() {
        NetworkConnection networkConnection=new NetworkConnection(SendEnquiry.this);
        if (networkConnection.CheckInternet()) {

        }else {
            getfailurealert("Please Turn On Mobile Data Or Connect To Wifi");
        }
    }

    private void sendEnquiry(String ename, String eemail, String emobile, String ecity, String epartno, String emessage) {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(SendEnquiry.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiService();
            // Calling JSON
            Call<SendEnquiryJson> call = service.SendEnquirty(ename,eemail,emobile,ecity,epartno,emessage);
            call.enqueue(new Callback<SendEnquiryJson>() {
                @Override
                public void onResponse(Call<SendEnquiryJson> call, Response<SendEnquiryJson> response) {
                    if (response.body().getResult().equals("Success")) {
                        progressDialog.dismiss();
                        getalert("Enquiry Details Submitted Successfully.");
                    } else {
                        progressDialog.dismiss();
                        getfailurealert("Please Try Again Later");
                    }
                }
                @Override
                public void onFailure(Call<SendEnquiryJson> call, Throwable t) {
                    progressDialog.dismiss();
                    getfailurealert("Failed to fetch data from server");
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
            getfailurealert("Exception error please try again later");
        }
    }

    private void getfailurealert(String alert) {
        alerterror=new AlertDialog.Builder(SendEnquiry.this).create();
        View orderplacedshow= LayoutInflater.from(SendEnquiry.this).inflate(R.layout.alertbox,null);
        TextView alerttext=orderplacedshow.findViewById(R.id.alerttext);
        TextView alertok=orderplacedshow.findViewById(R.id.alertok);
        alerttext.setText(alert);

        alertok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        alerterror.setView(orderplacedshow);
        alerterror.setCanceledOnTouchOutside(false);
        alerterror.show();
    }

    private void getalert(String alert) {
        alerterror=new AlertDialog.Builder(SendEnquiry.this).create();
        View orderplacedshow= LayoutInflater.from(SendEnquiry.this).inflate(R.layout.alertbox,null);
        TextView alerttext=orderplacedshow.findViewById(R.id.alerttext);
        TextView alertok=orderplacedshow.findViewById(R.id.alertok);
        alerttext.setText(alert);

        alertok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent home=new Intent(getApplicationContext(),MainActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
               startActivity(home);
            }
        });
        alerterror.setView(orderplacedshow);
        alerterror.setCanceledOnTouchOutside(false);
        alerterror.show();
    }

    public void Home(View view) {
        startActivity(new Intent(this, MainActivity.class));
    }

    public void Back(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
