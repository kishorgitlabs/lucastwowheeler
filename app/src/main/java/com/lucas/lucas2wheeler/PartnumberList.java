package com.lucas.lucas2wheeler;

import android.os.Bundle;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import adapter.PartNumberDetailsList;

public class PartnumberList extends AppCompatActivity {

    private ListView partlist;
    private PartNumberDetailsList partNumberDetailsList;
    private List<String> partnumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partnumber_list);
        partnumber=new ArrayList<>();
        partnumber.add("265653");
        partnumber.add("988988");
        partnumber.add("811445");
        partnumber.add("889456");
        partnumber.add("861156");
        partnumber.add("891615");

        partlist=findViewById(R.id.partlist);

        partNumberDetailsList=new PartNumberDetailsList(PartnumberList.this,partnumber);
        partlist.setAdapter(partNumberDetailsList);
    }
}
