package com.lucas.lucas2wheeler;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import APIInterface.CategoryAPI;
import RetroClient.RetroClient;
import adapter.ServicePartsAdapter;
import model.getchildpartsdetails.GetChildPartData;
import model.getchildpartsdetails.GetChildParts;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Service_Parts extends AppCompatActivity {

    private RecyclerView servicelist;
    private String servicePartNumber;
    private AlertDialog alerterror;
    private List<GetChildPartData> childPartData;
    private TextView servicePartno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service__parts);

        servicelist=findViewById(R.id.servicelist);
        servicePartno=findViewById(R.id.servicePartno);
        servicePartNumber=getIntent().getStringExtra("servicepartsnumber");
        servicePartno.setText(servicePartNumber);
//        LinearLayoutManager linearLayoutManager =new LinearLayout(Service_Parts.this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        servicelist.setLayoutManager(layoutManager);

        CheckInternet();
    }

    private void CheckInternet() {
        NetworkConnection networkConnection=new NetworkConnection(Service_Parts.this);
        if (networkConnection.CheckInternet()){
            getchildparts();
        }else {
            getfailurealert("Please Turn On Mobile Data Or Connect To Wifi");
        }
    }


    private void getchildparts() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(Service_Parts.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiService();
            // Calling JSON
            Call<GetChildParts> call = service.getChildParts(servicePartNumber);
            call.enqueue(new Callback<GetChildParts>() {
                @Override
                public void onResponse(Call<GetChildParts> call, Response<GetChildParts> response) {
                    if (response.body().getResult().equals("success")) {
                        progressDialog.dismiss();
                        childPartData=response.body().getData();
                        ServicePartsAdapter servicePartsAdapter=new ServicePartsAdapter(Service_Parts.this,childPartData,servicePartNumber);
                        servicelist.setAdapter(servicePartsAdapter);
                    } else {
                        progressDialog.dismiss();
                        getfailurealert("Please Try Again Later");
                    }
                }
                @Override
                public void onFailure(Call<GetChildParts> call, Throwable t) {
                    progressDialog.dismiss();
                    getfailurealert("Failed to fetch data from server");
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
            getfailurealert("Exception error please try again later");
        }
    }

    private void getfailurealert(String alert) {
        alerterror=new AlertDialog.Builder(Service_Parts.this).create();
        View orderplacedshow= LayoutInflater.from(Service_Parts.this).inflate(R.layout.alertbox,null);
        TextView alerttext=orderplacedshow.findViewById(R.id.alerttext);
        TextView alertok=orderplacedshow.findViewById(R.id.alertok);
        alerttext.setText(alert);

        alertok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        alerterror.setView(orderplacedshow);
        alerterror.setCanceledOnTouchOutside(false);
        alerterror.setCancelable(false);
        alerterror.show();
    }

    public void Home(View view) {
        startActivity(new Intent(this, MainActivity.class));
    }

    public void Back(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
